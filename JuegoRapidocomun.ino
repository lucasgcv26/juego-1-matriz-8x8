/*
Creador del programa: Lucas Gabriel Cerasa

matriz 8x8 con MAX7219. Din=12,Cs=10,Clk=11
potenciometro deslizable DTA= A0
A2 utilizado para generar numeros random
(se intento evitar el uso de FOR para no realentizar el programa)
*/

#include "LedControl.h"
LedControl lc=LedControl(12,11,10,1); //
#define POT A0

byte Caratriste[8]={
B01111110,
B10000001,
B10100101,
B10000001,
B10011001,
B10100101,
B10000001,
B01111110
};

byte Pantalla[8]={
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000,
  B00000000
};

int Y=0;
int Ubicacion;
int Ancho;
int Ganadas=0;
byte playerPos;
byte objectPos;

unsigned long Tref=0;
unsigned long vel=1000;
bool Bandera = true;

void setup(){
  lc.shutdown(0,false); //iniciar el display
  lc.setIntensity(0,4); //luminocidad
  lc.clearDisplay(0);
  randomSeed(analogRead(A2)); //hace que el dato usado para el random sea aleatorio
}

void loop() {
  if(Bandera){ //crea un nuevo objeto
    SpawnObject();
    Bandera = false;
    Tref= millis();
  }
  Player(); //ubica al jugador
  Mostrar(); //muestra el arreglo de bytes en la pantalla
  if(millis()-Tref>=vel){ //controla a que velocidas cae el objeto y verifica si se pierde o sigue
    if(Y>=7){ //situacion de posible choque
      Bandera=true;
      if(bitRead(playerPos,0) && bitRead(objectPos,0)||bitRead(playerPos,1) && bitRead(objectPos,1)||bitRead(playerPos,2) && bitRead(objectPos,2)||bitRead(playerPos,3) && bitRead(objectPos,3)||bitRead(playerPos,4) && bitRead(objectPos,4)||bitRead(playerPos,5) && bitRead(objectPos,5)||bitRead(playerPos,6) && bitRead(objectPos,6)||bitRead(playerPos,7) && bitRead(objectPos,7)){
        GameOver();
      }
      else{
          lc.setRow(0,7,(playerPos + objectPos));
          Tref=millis();
          vel-=10;
      }
    }
    else{
      if(Y<6){//
        Pantalla[Y+1]= Pantalla[Y];
      }
      else{Pantalla[7]=playerPos + objectPos;}
      Pantalla[Y]= B00000000;
      Y++;
      Tref=millis();
    }
  }
}

void Mostrar(){
  for(int i=0; i<8;i++){
    lc.setRow(0,i,Pantalla[i]); //0(dispositivo), fila, BYTE
  }
}

void Player(){
  int pos = map(analogRead(POT),0,1000,0,6);
  playerPos= 3*(int(pow(2,pos)+0.3));
  if(Y<7){
    Pantalla[7] = playerPos;
  }
}

void SpawnObject(){
  Y=0;
  Ancho = int(random(1,6));
  Ubicacion = int(random(0,(8-Ancho)));  
  int bin1;
  switch (Ancho) {
  case 1:
    bin1= 1;
    break;
  case 2:
    bin1= 3;
    break;
  case 3:
    bin1= 7;
    break;
  case 4:
    bin1= 15;
    break;
  case 5:
    bin1= 31;
    break;
  case 6:
    bin1= 63;
    break;
  }
  objectPos= bin1*(int(pow(2,Ubicacion)+0.3)); //suma 0.3 para que existiendo un error al pasar a int aproxime bien
  Pantalla[0] = objectPos;
}

void GameOver(){
  for(int i=0; i<8;i++){
    lc.setRow(0,i,Caratriste[i]); //0(dispositivo, fila, BYTE)
  }
  Ganadas=0;
  delay(1000);
  lc.clearDisplay(0);
  vel=1000;
}